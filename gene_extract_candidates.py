#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
import sys
import os
from text import util
from text.extractors.gene_extractor import extract_candidate_mentions
from text import extractor_util
from text.text_database import WriteDatabase, ReadDatabase

VERSION = '# ShallowFloat Gene Extractor 1.0.0'

def perform(text, out_db):
    words = util.read_text(text)
    mentions = extract_candidate_mentions(words)
    for gene in mentions:
        out_db.write(extractor_util.get_tsv_output(gene) + '\n')

def main(text, out_base_dir, out_name):
    with open(os.path.join(out_base_dir, out_name), 'w+') as out_db:
        perform(text, out_db)
