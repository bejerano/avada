package edu.stanford.bejerano;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;

public class TextLocator extends PDFTextStripper {

    private Set<String> stopwords = new HashSet<String>(Arrays.asList("me",
            "my", "myself", "we", "our", "ours", "ourselves", "you", "your",
            "yours", "yourself", "yourselves", "he", "him", "his", "himself",
            "she", "her", "hers", "herself", "it", "its", "itself", "they",
            "them", "their", "theirs", "themselves", "what", "which", "who",
            "whom", "this", "that", "these", "those", "am", "is", "are", "was",
            "were", "be", "been", "being", "have", "has", "had", "having", "do",
            "does", "did", "doing", "an", "the", "and", "but", "if", "or",
            "because", "as", "until", "while", "of", "by", "for", "with",
            "about", "against", "between", "into", "through", "during",
            "before", "after", "above", "below", "to", "from", "up", "down",
            "in", "out", "on", "off", "over", "under", "again", "further",
            "then", "once", "here", "there", "when", "where", "why", "how",
            "all", "any", "both", "each", "few", "more", "most", "other",
            "some", "such", "no", "nor", "not", "only", "own", "same", "so",
            "than", "too", "very", "can", "will", "just", "don", "should",
            "now"));

    private StringBuilder allPdfText;
    private List<String> allPdfTextList;
    private StringBuilder allPdfTextWithoutSpaces;
    private List<String> allPdfTextWithoutSpacesList;
    private List<Integer> charToWord;
    private Map<Integer, PDFTextLocation> wordToPosition;
    private int wordIdx;

    public TextLocator(PDDocument document) throws IOException {
        super.setSortByPosition(true);
        this.document = document;
        output = new Writer() {
            @Override
            public void write(char[] cbuf, int off, int len)
                    throws IOException {}

            @Override
            public void flush() throws IOException {}

            @Override
            public void close() throws IOException {}
        };
    }

    public List<PDFTextLocation> doSearch(Set<String> searchStrings,
            String pmid) throws IOException {
        allPdfText = new StringBuilder();
        allPdfTextList = new ArrayList<String>();
        allPdfTextWithoutSpaces = new StringBuilder();
        allPdfTextWithoutSpacesList = new ArrayList<String>();
        
        charToWord = new ArrayList<Integer>();
        wordToPosition = new HashMap<Integer, PDFTextLocation>();
        System.err.println("PMID " + pmid);
        try {
            processPages(document.getDocumentCatalog().getPages());
            String totalStringWithoutSpaces = allPdfTextWithoutSpaces.toString();
            // System.err.println(allPdfText);
            // System.err.println(totalStringWithoutSpaces);
            List<PDFTextLocation> locations = new ArrayList<>();
            for (String searchString : searchStrings) {
                boolean found = false;
                String searchStringWithoutSpace = searchString
                        .replaceAll("[^A-Za-z0-9]", "");
                // System.err
                //         .println("Search string: " + searchStringWithoutSpace);
                int charIdx = -1;
                do {
                    charIdx = totalStringWithoutSpaces.indexOf(searchStringWithoutSpace,
                            charIdx + 1);
                    // System.err.println("Char idx: " + charIdx);
                    if (charIdx < 0)
                        break;
                    found = true;
                    int searchWordIdx = charToWord.get(charIdx);
                    // System.err.println("Search word idx: " + searchWordIdx);
                    PDFTextLocation searchWordLocation = null;
                    try {
                        Object position = wordToPosition.get(searchWordIdx);
                        // System.err.println("Position: " + position);
                        if (position == null) {
                            System.err.println(
                                    "Have position null for search string"
                                            + searchStringWithoutSpace);
                            continue;
                        }
                        searchWordLocation = (PDFTextLocation) (((PDFTextLocation) position)
                                .clone());
                        // System.err.println("Search word idx word: "
                        //         + searchWordLocation.getText());
                    } catch (CloneNotSupportedException e) {
                        System.err.println("Clone not supported??");
                        e.printStackTrace();
                    }
                    searchWordLocation.setText(searchString);
                    searchWordLocation.setNumStopwordsLeft(allPdfTextList,
                            stopwords);
                    searchWordLocation.setNumStopwordsRight(allPdfTextList,
                            stopwords);
                    searchWordLocation.setNumLetterLeft(allPdfTextList);
                    searchWordLocation.setNumLetterRight(allPdfTextList);
                    locations.add(searchWordLocation);
                } while (charIdx >= 0);
                if (!found) {
                    System.err.println("Didn't find word " + searchString);
                }
            }
            return locations;
        } catch (IOException e) {
            System.err.println("IOException while processing pmid " + pmid
                    + " , returning empty locations list");
            e.printStackTrace();
            return new ArrayList<>();
        } catch (IndexOutOfBoundsException e) {
            System.err.println("PDFBox error: ");
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    protected void writeString(String text, List<TextPosition> textPositions)
            throws IOException {
        super.writeString(text);
        String pdfTextWithoutSpaces;
        pdfTextWithoutSpaces = text.replaceAll("[^A-Za-z0-9]", "");
        allPdfText.append(text);
        allPdfTextList.add(text);
        allPdfTextWithoutSpaces.append(pdfTextWithoutSpaces);
        allPdfTextWithoutSpacesList.add(pdfTextWithoutSpaces);

        PDFTextLocation textLoc = new PDFTextLocation();
        textLoc.setFound(true);
        textLoc.setPage(getCurrentPageNo());
        if (textPositions.size() > 0) {
            TextPosition pos = textPositions.get(0);
            textLoc.setX(pos.getXDirAdj());
            textLoc.setY(pos.getYDirAdj());
            textLoc.setOrientation(pos.getDir());
            textLoc.setWordIndex(wordIdx);
            textLoc.setText(pdfTextWithoutSpaces);
            wordToPosition.put(wordIdx, textLoc);
        }
        for (int i = 0; i < pdfTextWithoutSpaces.length(); i++) {
            charToWord.add(wordIdx);
        }
        wordIdx += 1;
    }
}
