#! /usr/bin/env python3

import sys
import re
import os
import array
import nltk
nltk.download('punkt')
from nltk.tokenize import sent_tokenize, word_tokenize
import sqlite3 as lite

import __settings__
from text.text_database import ReadDatabase, WriteDatabase, NotFoundException
from text import util


def read_sent(text):
    text = util.replace_some_greek_letters(text)
    text = replace_non_alnum_intelligent(text)
    return word_tokenize(text)


def replace_non_alnum_intelligent(line):
    ret = []
    for i in range(len(line)):
        char = line[i]
        if re.search(r'[(){}\[\]!.;,*/]', char):
            before = line[i - 1:i + 1]
            if before != 'c.' and before != 'p.':
                ret.append(' ')
                ret.append(char)
                ret.append(' ')
            else:
                ret.append(char)
        else:
            ret.append(char)
    ret = ''.join(ret)
    oldret = ret
    while oldret != ret:
        oldret = ret
        ret = ret.replace('  ', ' ')
    return ret


def process_text(all_text):
    url_regex = re.compile(r'[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)')
    all_text = url_regex.sub(' ', all_text, re.MULTILINE)

    sent_list = sent_tokenize(all_text)
    rv_sent = []
    for sent in sent_list:
        rv_sent.append(read_sent(sent))
    return rv_sent


def read_doc_encoding(filename, encoding):
    all_text = ''
    with open(filename, encoding=encoding) as f:
        for line in f:
            all_text += line
    return process_text(all_text)


def read_doc(filename):
    try:
        return read_doc_encoding(filename, "utf-8")
    except UnicodeDecodeError:
        return read_doc_encoding(filename, "ISO-8859-1")


def load_pmid_to_issn():
    rv = {}
    with open(os.path.join(__settings__.APP_HOME, 'onto/ref_sections/pmid_to_issn2.tsv')) as f:
        for line in f:
            line = line.strip().split('\t')
            rv[line[0]] = line[1]
    return rv


def load_split_issns():
    rv = set()
    with open(os.path.join(__settings__.APP_HOME, 'onto/ref_sections/split_by_refs_issns.txt')) as f:
        for line in f:
            rv.add(line.strip())
    return rv

# split_issns = load_split_issns()
conn = None
cur = None


def get_pmid_to_issn(pmid):
    global conn
    global cur
    if conn is None:
        conn = lite.connect(os.path.join(__settings__.APP_HOME, 'onto/ref_sections/pmid_to_issn.sqlite3'))
    if cur is None:
        cur = conn.cursor()
    cur.execute("SELECT issn FROM pmid_to_issn WHERE pmid = ?", [pmid])
    rows = [r for r in cur.fetchall()]
    if not rows:
        return None
    assert len(rows) == 1, (len(rows), rows)
    return str(rows[0][0])


def must_split_text(pmid):
    issn = get_pmid_to_issn(pmid)
    if issn is None:
        print("Have no ISSN for PMID %s" % pmid)
        return False
    return issn in split_issns

fam_names = None


def read_fam_names():
    # from pubmunch/lib/pubNlp.py
    global fam_names
    fam_names = set()
    fname = os.path.join(__settings__.APP_HOME, "onto/ref_sections/commonNames.txt")
    for l in open(fname, encoding='UTF-8'):
        fam_names.add(l.rstrip("\n").lower())
    fam_names.update(["references", "bibliography", "literature", "refereces"])


def skip_forw_max(text, start, max_dist):
    # from pubmunch/lib/pubNlp.py
    for i in range(start, min(start + max_dist, len(text))):
        if text[i] in ["\a", "\n", "\r"]:
            return i
    return start


# from pubmunch/lib/pubNlp.py
wordRe = re.compile("[\w_]+")


def word_splitter(text):
    # from pubmunch/lib/pubNlp.py
    for match in wordRe.finditer(text):
        word = match.group()
        start, end = match.start(), match.end()
        yield start, end, word


def find_runs(arr, min_length):
    rv = []
    size = 0
    run_start = None
    for i in range(0, len(arr)):
        if arr[i] == 1:
            size += 1
            if not run_start:
                run_start = i
        else:
            if size >= min_length:
                rv.append((run_start, i))
            size = 0
            run_start = None
    if size >= min_length:
        rv.append((run_start, i))

    return rv


def find_ref_section(text, name_ext=100, min_length=1700):
    # from pubmunch/lib/pubNlp.py
    if not fam_names:
        read_fam_names()

    mask = array.array("b", len(text) * [0])

    for start, end, word in word_splitter(text.lower()):
        if word not in fam_names:
            continue
        left_box = max(0, start - name_ext)
        right_box = min(len(text), end + name_ext)

        for i in range(left_box, right_box):
            mask[i] = 1

    ref_starts_ends = find_runs(mask, min_length)

    return ref_starts_ends


def get_between(starts_ends, text):
    if not starts_ends:
        return [(0, len(text))]
    last_end = 0
    rv = []
    for start, end in starts_ends:
        rv.append((last_end, start))
        last_end = end
    rv.append((end, len(text)))
    return rv

non_alnum = re.compile('[^A-Za-z]+')


def split_if_necessary(pmid, text, titles_db):
    if not must_split_text(pmid):
        return text
    try:
        print("Trying to split PMID %s" % pmid)
        title = next(titles_db.query(pmid))
        text = '\a'.join(text)

        ref_starts_ends = find_ref_section(text)
        between_refs = get_between(ref_starts_ends, text)
        found = False
        found_idx = None

        for i, (start, end) in enumerate(between_refs):
            between_text = text[start:end]
            between_text_non_alnum = non_alnum.sub('', between_text.replace('\n', '').replace('\a', '')).lower()
            title_non_alnum = non_alnum.sub('', title.replace('\n', '').replace('\a', '')).lower()
            if title_non_alnum in between_text_non_alnum \
                    or title_non_alnum[:int(len(title_non_alnum) / 2)] in between_text_non_alnum \
                    or title_non_alnum[int(len(title_non_alnum) / 2):] in between_text_non_alnum:
                found = True
                found_idx = i
                break
        if not found:
            return text
        else:
            print("Taking index %d" % i)
            start, end = between_refs[found_idx]
            return text[start:end].split('\a')
    except NotFoundException:
        return text


def main(text, out_base_dir, out_name, split_full_text):
    #titles_db = None
    with open(os.path.join(out_base_dir, out_name), 'w+') as out_db:
        #if split_full_text:
         #   if titles_db is None:
          #      print(os.environ.get("NCBI_SOLVER_DATA_DIR"))
           #     print(__settings__.DATA_DIR)
            #    print(__settings__.RAW_MEDLINE_TITLES_DATABASE_NAME)
             #   titles_db = ReadDatabase(__settings__.DATA_DIR,
              #                               __settings__.RAW_MEDLINE_TITLES_DATABASE_NAME)
           # text = split_if_necessary(pmid, text, titles_db)

        #out_db.set_pmid(pmid, source)
        concat_text = ''
        for line in text:
            concat_text += ' ' + line
        for sent in process_text(concat_text):
            sent = ' '.join(sent)
            out_db.write(sent + "\n")

    #if titles_db is not None:
     #   titles_db.close()
    #if conn is not None:
     #   conn.close()


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
