#! /usr/bin/env python3
# from __future__ import print_function

import sys, os
from collections import defaultdict, namedtuple
from math import sqrt, acos
import sqlite3 as lite

from text.text_database import ReadDatabase
import text.tuples as tuples
import util
import __settings__


def vector_length(v):
    return sqrt(v[0] * v[0] + v[1] * v[1])


def euclidean(p1, p2):
    v = (float(p2.x) - float(p1.x), float(p2.y) - float(p1.y))
    return vector_length(v)


def inv_cos_angle(p1, p2):
    v = (float(p2.x) - float(p1.x), float(p2.y) - float(p1.y))
    vlen = vector_length(v)
    if vlen == 0:
        return 0
    cosphi = (float(p2.y) - float(p1.y)) / vector_length(v)
    return acos(cosphi)


RelativePosition2D = namedtuple("RelativePosition2D", ["page_dist", "dist", "angle", "relx",
                                                       "rely", "above", "lstop1", "rstop1",
                                                       "lletter1", "rletter1", "lstop2", "rstop2",
                                                       "lletter2", "rletter2"])
noneRelPos2D = RelativePosition2D(page_dist=0, dist=0, angle=0, relx=0, rely=0, above=float(False),
                                  lstop1=0, rstop1=0, lletter1=0,
                                  rletter1=0, lstop2=0, rstop2=0, lletter2=0, rletter2=0)

RelativePositionWord = namedtuple("RelativePositionWord", ["dist", "above", "lstop1", "rstop1",
                                                           "lletter1", "rletter1", "lstop2", "rstop2",
                                                           "lletter2", "rletter2"])
noneRelPosWord = RelativePositionWord(dist=0, above=float(False),
                                      lstop1=0, rstop1=0, lletter1=0, rletter1=0,
                                      lstop2=0, rstop2=0, lletter2=0, rletter2=0)


def relationship_2d(gpos, vpos):
    page_dist = int(gpos.page) - int(vpos.page)
    dist = euclidean(gpos, vpos)
    angle = inv_cos_angle(gpos, vpos)
    relx = float(gpos.x) - float(vpos.x)
    rely = float(gpos.y) - float(vpos.y)
    if rely <= 20:
        above = float(True)
    else:
        above = float(False)
    # topical gene score of gene is added when function is called
    return RelativePosition2D(page_dist=page_dist, dist=dist, angle=angle,
                              relx=relx, rely=rely, above=above,
                              lstop1=gpos.num_stopwords_left, rstop1=gpos.num_stopwords_right,
                              lletter1=gpos.num_letter_left, rletter1=gpos.num_letter_right,
                              lstop2=vpos.num_stopwords_left, rstop2=vpos.num_stopwords_right,
                              lletter2=vpos.num_letter_left, rletter2=vpos.num_letter_right)


def relationship_wordspace(gpos, vpos):
    # topical gene score of gene is added when function is called
    return RelativePositionWord(dist=abs(float(gpos.wordidx) - float(vpos.wordidx)),
                                above=float(float(gpos.wordidx) - float(vpos.wordidx) < 0),
                                lstop1=gpos.num_stopwords_left, rstop1=gpos.num_stopwords_right,
                                lletter1=gpos.num_letter_left, rletter1=gpos.num_letter_right,
                                lstop2=vpos.num_stopwords_left, rstop2=vpos.num_stopwords_right,
                                lletter2=vpos.num_letter_left, rletter2=vpos.num_letter_right)


def get_gene_names(pmid, ensembl_ids, refseq_id):
    #full_gms = tuples.read_text(full_genes_db.query(pmid), tuples.geneMentionParser,
    #                            tuples.GeneMention, ignoreTypeErrors=True)
    full_names = set()
    #for gm in full_gms:
    #    if set(ensembl_ids).intersection(set(gm.eids)):
    #        full_names.update([x.strip() for x in gm.orig_words.split('|^|')])

    with open(os.path.join(os.path.dirname(__file__), 'candidate_genes.tsv'), 'r') as genes:
        ensembl_genes = set()
        words = set()
        for line in genes:
            eids = line.split("\t")[5]
            orig_words = line.split("\t")[8]
            for eid in eids.split(" "):
                ensembl_genes.add(eid)
            for word in orig_words.split("|^|"):
                words.add(word)
        if set(ensembl_ids).intersection(ensembl_genes):
            full_names.update([x.strip() for x in words])

    all_ids = set(ensembl_ids).union(set([refseq_id])).union(full_names)
    # print("All gene names for eids %s: %s" % (ensembl_ids, all_ids), file=sys.stderr)
    # uppercase_ids = set(x for x in all_ids if x.isupper() and len(x.split()) == 1)
    # return uppercase_ids
    return all_ids


positions_conn = lite.connect(os.path.join(os.path.dirname(__file__), "gene_variant_positions.sqlite3"))
positions_cur = positions_conn.cursor()

PdfPosition = namedtuple('PdfPosition', ['pmid', 'name', 'page', 'x', 'y', 'orientation',
                                         'wordidx', 'num_stopwords_left', 'num_stopwords_right',
                                         'num_letter_left', 'num_letter_right'])


def get_pdf_positions(orig_str, filename):
    rv = []
    with open(os.path.join(os.path.dirname(__file__), filename)) as f:
        for line in f:
            # print(row)
            # originally named, num_non_letter, the number in reality is num_letter
            name, page, x, y, orientation, wordidx, num_stopwords_left, \
            num_stopwords_right, num_letter_left, num_letter_right = line.split("\t")
            if name == orig_str:
                rv.append(PdfPosition(pmid="", name=name, page=page, x=x, y=y,
                              orientation=orientation,
                              wordidx=wordidx, num_stopwords_left=num_stopwords_left,
                              num_stopwords_right=num_stopwords_right,
                              num_letter_left=num_letter_left,
                              num_letter_right=num_letter_right))
    return rv


def flatten(l):
    return [x for y in l for x in y]


def double_flatten(l):
    return [x for y in l for x in y[0]], [x for y in l for x in y[1]]


patho_variant_positive_training_set = set()
patho_variant_pmids = set()


def load_training_data(filename, allowed_pmids):
    print("Loading patho_variant data", file=sys.stderr)
    for chrom, start, ref, alt, pmid in util.read_tsv(filename):
        if allowed_pmids is not None and pmid not in allowed_pmids:
            continue
        patho_variant_positive_training_set.add(("chr" + chrom, start, ref, alt, pmid))
        patho_variant_pmids.add(pmid)
    print("Loaded patho_variant data", file=sys.stderr)
    sys.stderr.flush()


def find_closest_above_2d(rel_positions):
    rv_list = [x for x in sorted(rel_positions, key=lambda relpos: relpos.dist) if x.above and x.page_dist == 0]
    feature_names = ["found_closest_above_2d"] + ["%s_above_2d" % name for name, _ in noneRelPos2D._asdict().items()]
    if rv_list:
        return feature_names, [float(True)] + [x for x in rv_list[0]], rel_positions[rv_list[0]]
    return feature_names, [float(False)] + [x for x in noneRelPos2D], None


def find_closest_above_wordspace(rel_positions):
    rv_list = [x for x in sorted(rel_positions, key=lambda relpos: relpos.dist) if x.above]
    feature_names = ["found_closest_above_wordspace"] + ["%s_above_wordspace" % name for name, _ in noneRelPosWord._asdict().items()]
    if rv_list:
        return feature_names, [float(True)] + [x for x in rv_list[0]], rel_positions[rv_list[0]]
    return feature_names, [float(False)] + [x for x in noneRelPosWord], None


def find_closest_2d(rel_positions):
    rv_list = [x for x in sorted(rel_positions, key=lambda relpos: relpos.dist) if x.page_dist == 0]
    feature_names = ["found_closest_2d"] + ["%s_closest_2d" % name for name, _ in noneRelPos2D._asdict().items()]
    if rv_list:
        return feature_names, [float(True)] + [x for x in rv_list[0]], rel_positions[rv_list[0]]
    return feature_names, [float(False)] + [x for x in noneRelPos2D], None


def find_closest_wordspace(rel_positions):
    rv_list = [x for x in sorted(rel_positions, key=lambda relpos: relpos.dist)]
    feature_names = ["found_closest_wordspace"] + ["%s_closest_wordspace" % name for name, _ in noneRelPosWord._asdict().items()]
    if rv_list:
        return feature_names, [float(True)] + [x for x in rv_list[0]], rel_positions[rv_list[0]]
    return feature_names, [float(False)] + [x for x in noneRelPosWord], None


pmid_eid_to_right_gene_score = defaultdict(lambda: 0.0)


def load_pmid_eid_to_right_gene_score():
    with open(os.path.join(os.path.dirname(__file__), 'filtered_right_genes.tsv')) as f:
        for line in f:
            eid, proba = line.strip('\n').split('\t')
            pmid_eid_to_right_gene_score[eid] = float(proba)


def get_right_gene_score(eids):
    if eids is None:
        return 0.0
    right_gene_score = max(pmid_eid_to_right_gene_score[eid] for eid in eids)
    # print("For eids %s, have right gene score %f" % (eids, right_gene_score), file=sys.stderr)
    return right_gene_score


# fs is for features_suffix
def get_features_closest_above_2d(vpos, eid_gposs, pmid, fs):
    relpos_to_eids = {relationship_2d(gpos, vpos): eids for gpos, eids in eid_gposs.items()}
    feature_names, closest_above, eids = find_closest_above_2d(relpos_to_eids)
    right_gene_score = get_right_gene_score(eids)
    return [x + fs for x in feature_names] + ["closest_above_2d_right_gene_score" + fs], closest_above + [right_gene_score]


def get_features_closest_above_wordspace(vpos, eid_gposs, pmid, fs):
    relpos_to_eids = {relationship_wordspace(gpos, vpos): eids for gpos, eids in eid_gposs.items()}
    feature_names, closest_above, eids = find_closest_above_wordspace(relpos_to_eids)
    right_gene_score = get_right_gene_score(eids)
    return [x + fs for x in feature_names] + ["closest_above_wordspace_right_gene_score" + fs],  closest_above + [right_gene_score]


def get_features_closest_2d(vpos, eid_gposs, pmid, fs):
    relpos_to_eids = {relationship_2d(gpos, vpos): eids for gpos, eids in eid_gposs.items()}
    feature_names, closest, eids = find_closest_2d(relpos_to_eids)
    right_gene_score = get_right_gene_score(eids)
    return [x + fs for x in feature_names] + ["closest_2d_right_gene_score" + fs], closest + [right_gene_score]


def get_features_closest_wordspace(vpos, eid_gposs, pmid, fs):
    relpos_to_eids = {relationship_wordspace(gpos, vpos): eids for gpos, eids in eid_gposs.items()}
    feature_names, closest, eids = find_closest_wordspace(relpos_to_eids)
    right_gene_score = get_right_gene_score(eids)
    return [x + fs for x in feature_names] + ["closest_wordspace_right_gene_score" + fs], closest + [right_gene_score]


def create_feature_groups(vpos, eid_gposs, all_gposs, pmid, eid):
    # fg is "feature group"
    fgs = list()
    fgs.append(get_features_closest_above_2d(vpos, eid_gposs, pmid, "_eid"))
    fgs.append(get_features_closest_above_wordspace(vpos, eid_gposs, pmid, "_eid"))
    fgs.append(get_features_closest_2d(vpos, eid_gposs, pmid, "_eid"))
    fgs.append(get_features_closest_wordspace(vpos, eid_gposs, pmid, "_eid"))
    fgs.append(get_features_closest_above_2d(vpos, all_gposs, pmid, "_all"))
    fgs.append(get_features_closest_above_wordspace(vpos, all_gposs, pmid, "_all"))
    fgs.append(get_features_closest_2d(vpos, all_gposs, pmid, "_all"))
    fgs.append(get_features_closest_wordspace(vpos, all_gposs, pmid, "_all"))
    fgs.append((["eid_right_gene_score"], [get_right_gene_score([eid])]))

    relpos_to_eids = {relationship_2d(gpos, vpos): eids for gpos, eids in eid_gposs.items()}
    _, closest_above_2d_eid, _ = find_closest_above_2d(relpos_to_eids)
    _, closest_2d_eid, _ = find_closest_2d(relpos_to_eids)
    _, closest_above_wordspace_eid, _ = find_closest_above_wordspace(relpos_to_eids)
    _, closest_wordspace_eid, _ = find_closest_wordspace(relpos_to_eids)

    relpos_to_eids = {relationship_2d(gpos, vpos): eids for gpos, eids in all_gposs.items()}
    _, closest_above_2d_all, _ = find_closest_above_2d(relpos_to_eids)
    _, closest_2d_all, _ = find_closest_2d(relpos_to_eids)
    _, closest_above_wordspace_all, _ = find_closest_above_wordspace(relpos_to_eids)
    _, closest_wordspace_all, _ = find_closest_wordspace(relpos_to_eids)

    fgs.append((["closest_above_2d_eid=closest_2d_eid",
                "closest_above_2d_eid=closest_above_2d_all",
                "closest_above_2d_eid=closest_2d_all",
                "closest_2d_eid=closest_above_2d_all",
                "closest_2d_eid=closest_2d_all",
                "closest_2d_above_all=closest_2d_all",
                "closest_above_wordspace_eid=closest_wordspace_eid",
                "closest_above_wordspace_eid=closest_above_wordspace_all",
                "closest_above_wordspace_eid=closest_wordspace_all",
                "closest_wordspace_eid=closest_above_wordspace_all",
                "closest_wordspace_eid=closest_wordspace_all",
                "closest_wordspace_above_all=closest_wordspace_all"
                ], 
                [float(x) for x in
                 [closest_above_2d_eid==closest_2d_eid,
                  closest_above_2d_eid==closest_above_2d_all,
                  closest_above_2d_eid==closest_2d_all,
                  closest_2d_eid==closest_above_2d_all,
                  closest_2d_eid==closest_2d_all,
                  closest_above_2d_all==closest_2d_all,
                  closest_above_wordspace_eid==closest_wordspace_eid,
                  closest_above_wordspace_eid==closest_above_wordspace_all,
                  closest_above_wordspace_eid==closest_wordspace_all,
                  closest_wordspace_eid==closest_above_wordspace_all,
                  closest_wordspace_eid==closest_wordspace_all,
                  closest_above_wordspace_all==closest_wordspace_all]]))
    feature_names, fgs = double_flatten(fgs)
    yield feature_names, fgs


def create_gposs_to_eid(eid_to_gposs, eids):
    rv = defaultdict(lambda: set())
    for eid in eids:
        for gpos in eid_to_gposs[eid]:
            rv[gpos].add(eid)
    return rv


def print_training_set(pmid, orig_str_to_vposs, orig_str_to_eids, eid_to_gposs, true_orig_str_to_eids):
    for orig_str, vposs in orig_str_to_vposs.items():
        if len(vposs) == 0:
            # print("PMID %s: Missing variant position for variant %s" % (pmid, orig_str), file=sys.stderr)
            continue
        if orig_str not in true_orig_str_to_eids:
            continue
        eids = set(orig_str_to_eids[orig_str])
        all_gposs = create_gposs_to_eid(eid_to_gposs, eids)
        for eid in eids:
            array = []
            eid_gposs = create_gposs_to_eid(eid_to_gposs, [eid])
            if len(eid_gposs) == 0:
                # print("PMID %s: Missing gene position for gene %s" % (pmid, eid), file=sys.stderr)
                continue
            for vpos in vposs:
                # for feature_names, fg in create_feature_groups(vpos, eid_gposs, all_gposs, pmid, eid):
                #     print(feature_names)
                #     sys.exit(1)
                array.extend([fg for feature_names, fg in create_feature_groups(vpos, eid_gposs, all_gposs, pmid, eid)])
            if len(array) == 0:
                continue
            str_array = []
            for features in array:
                str_array.append(",".join(str(x) for x in features))
            if eid in true_orig_str_to_eids[orig_str]:
                label = 1.0
            else:
                label = 0.0
            print("%f\t%s\t%s\t%s\t%s" % (label, pmid, "|".join(str_array), orig_str, eid))
        sys.stdout.flush()


def create_supervision_data(pmid, last_pmid_orig_str_to_eid_vcf):
    rv = defaultdict(lambda: set())
    for orig_variant_st, infos in last_pmid_orig_str_to_eid_vcf.items():
        for (chrom, vcf_pos, vcf_ref, vcf_alt, eids) in infos:
            if (chrom, vcf_pos, vcf_ref, vcf_alt, pmid) in patho_variant_positive_training_set:
                rv[orig_variant_st].update(eids)
    return rv


def main():
    extracted_filename = sys.argv[1]
    full_genes_basedir = sys.argv[2]
    full_genes_name = sys.argv[3]
    training_data_filename = sys.argv[4]
    dateX = sys.argv[5]
    dateY = sys.argv[6]
    full_genes_db = ReadDatabase(full_genes_basedir, full_genes_name)
    load_pmid_eid_to_right_gene_score()

    last_pmid = None  # done
    last_pmid_eid_to_gposs = None  # done
    last_pmid_orig_str_to_vposs = None  # done
    last_pmid_orig_str_to_eids = None  # done
    last_pmid_orig_str_to_eid_vcf = None  # done

    if dateX == "all":
        allowed_pmids_tmp = None
    else:
        allowed_pmids_tmp = []
        with open(os.path.join(os.environ['NCBI_SOLVER_HOME'], 'time_machine', dateX, 'allowed_pmids.tsv')) as f:
            for line in f:
                allowed_pmids_tmp.append(line.rstrip('\n'))
        allowed_pmids_tmp = set(allowed_pmids_tmp)
    allowed_pmids = allowed_pmids_tmp

    load_training_data(training_data_filename, allowed_pmids)
    for pmid, chrom, start, end, info, length, strand, \
            _, _, _, _, _, _, orig_variant_str, vcf_pos, \
            vcf_ref, vcf_alt, refseq_info in util.read_tsv(extracted_filename):
        if vcf_ref == "None":
            continue
        info = info.split(',')
        if pmid not in patho_variant_pmids:
            continue
        print("PMID %s, variant %s, (%s, %s, %s, %s)" % (pmid, orig_variant_str, chrom, vcf_pos, vcf_ref, vcf_alt),
              file=sys.stderr)
        sys.stderr.flush()
        if pmid != last_pmid:
            if last_pmid_eid_to_gposs:
                true_orig_str_to_eids = create_supervision_data(last_pmid, last_pmid_orig_str_to_eid_vcf)
                print_training_set(pmid=last_pmid,
                                   orig_str_to_vposs=last_pmid_orig_str_to_vposs,
                                   orig_str_to_eids=last_pmid_orig_str_to_eids,
                                   eid_to_gposs=last_pmid_eid_to_gposs,
                                   true_orig_str_to_eids=true_orig_str_to_eids)
            last_pmid = pmid
            last_pmid_eid_to_gposs = defaultdict(lambda: set())
            last_pmid_orig_str_to_eid_vcf = defaultdict(lambda: set())
            last_pmid_orig_str_to_vposs = {}
            last_pmid_orig_str_to_eids = defaultdict(lambda: set())
        eids = tuple(info[1].split('|'))
        refseq_id = info[5].split('.')[0]
        if orig_variant_str not in last_pmid_orig_str_to_vposs:
            variant_positions = get_pdf_positions(pmid, orig_variant_str, positions_cur)
            last_pmid_orig_str_to_vposs[orig_variant_str] = variant_positions
        gene_names = get_gene_names(pmid, eids, refseq_id, full_genes_db)
        # print("Gene names filtered: %s" % gene_names, file=sys.stderr)
        for eid in eids:
            for gene_name in gene_names:
                last_pmid_eid_to_gposs[eid].update(get_pdf_positions(pmid, gene_name, positions_cur))
        t = (chrom, vcf_pos, vcf_ref, vcf_alt, eids)
        last_pmid_orig_str_to_eid_vcf[orig_variant_str].add(t)
        last_pmid_orig_str_to_eids[orig_variant_str].update(eids)


if __name__ == "__main__":
    main()
