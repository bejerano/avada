import os

APP_HOME = os.environ.get("NCBI_SOLVER_HOME",
                          "/Users/jbirgmei/Stanford/Bejerano/ncbi-solver")
DATA_DIR = os.environ.get("NCBI_SOLVER_DATA_DIR",
                          "/Users/jbirgmei/Stanford/Bejerano/text")
GENES_DATABASE_NAME = os.environ.get("NCBI_SOLVER_GENES_DATABASE_NAME",
                                     "pubmunch_genes")
PHENOS_DATABASE_NAME = os.environ.get("NCBI_SOLVER_PHENOS_DATABASE_NAME",
                                      "pubmunch_phenos_without_acronyms")
RIGHT_GENES_DATABASE_NAME = os.environ.get("NCBI_SOLVER_RIGHT_GENE_DATABASE_NAME",
                                           "pubmunch_right_genes")
INHERITANCE_MODE_DATABASE_NAME = os.environ.get("NCBI_SOLVER_INHERITANCE_MODE_DATABASE_NAME",
                                                "medline_inheritance_mode")

RAW_TEXT_DATABASE_NAME = os.environ.get("NCBI_SOLVER_PROCESSED_TEXT_DATABASE_NAME",
                                        "pubmunch_raw")
PROCESSED_TEXT_DATABASE_NAME = os.environ.get("NCBI_SOLVER_PROCESSED_TEXT_DATABASE_NAME",
                                              "pubmunch_processed")
RAW_PUBMUNCH_DATABASE_NAME = os.environ.get("NCBI_SOLVER_PROCESSED_TEXT_DATABASE_NAME",
                                            "pubmunch_raw")
PROCESSED_PUBMUNCH_DATABASE_NAME = os.environ.get("NCBI_SOLVER_PROCESSED_TEXT_DATABASE_NAME",
                                                  "pubmunch_processed")
PROCESSED_MEDLINE_TITLES_DATABASE_NAME = os.environ.get("NCBI_SOLVER_PROCESSED_MEDLINE_TITLES_DATABASE_NAME",
                                                        "medline_titles_processed")
PROCESSED_MEDLINE_ABSTRACTS_DATABASE_NAME = os.environ.get("NCBI_SOLVER_PROCESSED_MEDLINE_ABSTRACTS_DATABASE_NAME",
                                                           "medline_abstracts_processed")
RAW_MEDLINE_TITLES_DATABASE_NAME = os.environ.get("NCBI_SOLVER_RAW_MEDLINE_TITLES_DATABASE_NAME",
                                                  "medline_titles_raw")
RAW_MEDLINE_ABSTRACTS_DATABASE_NAME = os.environ.get("NCBI_SOLVER_RAW_MEDLINE_ABSTRACTS_DATABASE_NAME",
                                                     "medline_abstracts_raw")
MEDLINE_TITLES_GENES_DATABASE_NAME = os.environ.get("NCBI_SOLVER_MEDLINE_TITLES_GENES_DATABASE_NAME",
                                                    "medline_titles_genes")
MEDLINE_TITLES_PHENOS_DATABASE_NAME = os.environ.get("NCBI_SOLVER_MEDLINE_TITLES_PHENOS_DATABASE_NAME",
                                                     "medline_titles_phenos_without_acronyms")
MEDLINE_ABSTRACTS_GENES_DATABASE_NAME = os.environ.get("NCBI_SOLVER_MEDLINE_ABSTRACTS_GENES_DATABASE_NAME",
                                                       "medline_abstracts_genes")
MEDLINE_ABSTRACTS_PHENOS_DATABASE_NAME = os.environ.get("NCBI_SOLVER_MEDLINE_ABSTRACTS_PHENOS_DATABASE_NAME",
                                                        "medline_abstracts_phenos_without_acronyms")