#! /usr/bin/env python

import sys
import os
from collections import defaultdict
#import codecs
import re

import text.tuples as tuples
import text_to_wordlines
import gene_extract_candidates
import right_genes_extractor

from text.text_database import ReadDatabase
from variants.seq_data import SeqData

from pm_lib.varFinder import findVariantDescriptions, groundVariants

pmid_to_right_genes = defaultdict(lambda: [])
ensembl_to_entrez = {}
with open(os.path.join(os.path.dirname(__file__), 'data/gene.good_ensembl.entrez.map')) as f:
    for line in f:
        line = line.strip().split()
        ensembl_id = line[0]
        entrez_id = line[1]
        ensembl_to_entrez[ensembl_id] = entrez_id

def load_pmid_to_right_genes(right_gene_to_pmids_filename):
    global pmid_to_right_genes
    with open(right_gene_to_pmids_filename) as f:
        for line in f:
            line = line.rstrip('\n').split('\t')
            ensembl_id = line[0]
            print(ensembl_id)
            pmids = [x.split(':')[0] for x in line[1].split()]
            for pmid in pmids:
                pmid_to_right_genes[pmid].append(ensembl_id)

if __name__ == "__main__":
    text = sys.argv[1]
    out_base_dir = sys.argv[2]
    out_name = sys.argv[3]
    title_start = 0
    title_end = 5
    abstract_start = 0
    abstract_end = 20

    # right_gene_to_pmids_filename = sys.argv[4]
    # load_pmid_to_right_genes(right_gene_to_pmids_filename)
    geneData = SeqData()
    
    # ensembl_genes = pmid_to_right_genes[pmid]
    # not sure about the below, maybe needs to be rewritten to not be dependent on pmid
    text_to_wordlines.main(text.split('\n'), os.path.dirname(__file__), 'temp.tsv', 'True')
    new_text = ""
    with open(os.path.join(os.path.dirname(__file__), 'temp.tsv'), 'r') as text_file:
        for line in text_file.readlines():
            new_text += line
            
    gene_extract_candidates.main(new_text.split('\n')[title_start:title_end], 
            os.path.dirname(__file__), 'title_genes.tsv')
    gene_extract_candidates.main(new_text.split('\n')[abstract_start:abstract_end], 
            os.path.dirname(__file__), 'abstract_genes.tsv')
    gene_extract_candidates.main(new_text.split('\n'), 
            os.path.dirname(__file__), 'candidate_genes.tsv')
    
    ensembl_genes = set()
    with open(os.path.join(os.path.dirname(__file__), 'candidate_genes.tsv'), 'r') as genes:
        for line in genes:
            eids = line.split("\t")[5]
            for eid in eids.split(" "):
                ensembl_genes.add(eid)
    
    # print(ensembl_genes)
    entrez_genes = set([ensembl_to_entrez[eid] for eid in ensembl_genes if eid in ensembl_to_entrez])
    # print(genes)
    entrez_to_ensembl = defaultdict(lambda: set())
    for eid in ensembl_genes:
        if eid in ensembl_to_entrez:
            entrez_to_ensembl[ensembl_to_entrez[eid]].add(eid)
    right_genes_extractor.main("_all", os.path.dirname(__file__), "right_genes.tsv")
    
    one_string_text = '\n'.join([x for x in new_text])
    typeToDescription = findVariantDescriptions(text, docId="")
    with open('type_to_description.txt', 'w+') as f:
        for entry in typeToDescription:
            f.write(str(entry) + "\n")
            f.write(str(typeToDescription.get(entry, [])) + "\n")
    dbSnp = typeToDescription.get("dbSnp", [])
    all_beds = []
    for variantType in typeToDescription:
        for variant, mentions in typeToDescription[variantType]:
            beds = groundVariants("", text, variant, mentions,
                                      entrez_to_ensembl, uncheckable_rv=True)
            small_enough_beds = [x for x in beds if int(x[2]) - int(x[1]) <= 20]
            all_beds.extend([x for x in small_enough_beds])
    with open(os.path.join(os.path.dirname(__file__), 'gene_var_mappings.tsv'), 'w+') as mapping:
        mapping.write('\n'.join('\t'.join(x) for x in all_beds))
